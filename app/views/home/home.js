'use strict';

/**
 * @ngdoc function
 * @name riteshDemoApp.controller:AuthenticationCtrl
 * @description
 * # AuthenticationCtrl
 * Controller of the riteshDemoApp
 */
angular.module('riteshDemoApp')
  .controller('HomeCtrl', function ( $scope, $state, blockUI, weatherservice) {
    
    $scope.objWeather = {};
    $scope.arrWeather = [];
    $scope.labels = [];
    $scope.arrWeatherForecast = [];
    $scope.datasetOverride = [
      {
        label: "Temprature",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line'
      }
    ];

    //get weather info
    $scope.get_weather_details = function(city){
      try{
        blockUI.start();
        $scope.arrWeather = []; /*Re-initlization*/
        $scope.labels = []; //clear chart objects
        $scope.arrWeatherForecast = []; //clear chart objects
        weatherservice.get_weather_details(city).then(function (response) {
          if (response.status == 200) 
            $scope.objWeather = response.data;
        });
        blockUI.stop();
      } catch(e){
        console.error(e);
      }
    }

    //return last five days data
    $scope.get_week_data = function(city){
    	try{
    		blockUI.start();
    		weatherservice.get_week_data(city).then(function (response) {
          var arrTemp = [];
          if (response.status == 200) {
            $scope.arrWeather = response.data;
            $scope.arrWeather.list.map((value, key)=> {
              $scope.labels.push($scope.parse_date(value.dt));
              arrTemp.push(value["temp"]["day"]);
            });
            $scope.arrWeatherForecast.push(arrTemp);                    
          } 
        });
        blockUI.stop();
    	} catch(e){
        console.error(e);
    	}
    }

    //parse date number to actual date
    $scope.parse_date = function(dt){
      try{
        var date = new Date(dt * 1000);
        var dd = date.getDate(),
            mm = date.getMonth() + 1, 
            yy = date.getFullYear();
        return dd + "-" + mm + "-" + yy;
      } catch(e){
        console.error(e);
      }
    }
});