'use strict';

/**
 * @ngdoc function
 * @name riteshDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the riteshDemoApp
 */
angular.module('riteshDemoApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
