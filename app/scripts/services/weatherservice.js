'use strict';

/**
 * @ngdoc service
 * @name riteshDemoApp.weatherservice
 * @description
 * # weatherservice
 * Service in the riteshDemoApp.
 */
angular.module('riteshDemoApp')
                        
  .service('weatherservice', function ($log, $http, $q, apiEndpoint, apiEndpoint1) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    //alert('weatherservice calling...');

    this.get_weather_details = function(city){
    	try{
    		var queryString = '';
    		if(city != ''){
    			queryString = queryString + "&q=" + city;
    		}
    		var deferred = $q.defer();
    		return $http.get(apiEndpoint1 + queryString).then(function successCallback(response){
		        deferred.resolve(response);
		        return response;
	        }, function errorCallback(response){
		        deferred.resolve(response);
		        return response;
      		});
    	} catch(e){
    		console.error(e);
    	}
    }

    this.get_week_data = function(city){
    	try{
    		var queryString = '';
    		if(city != ''){
    			queryString = queryString + "&q=" + city;
    		}
    		var deferred = $q.defer();
    		return $http.get(apiEndpoint + queryString).then(function successCallback(response){
		        deferred.resolve(response);
		        return response;
	        }, function errorCallback(response){
		        deferred.resolve(response);
		        return response;
      		});
    	} catch(e){
    		console.error(e);
    	}
    }
});