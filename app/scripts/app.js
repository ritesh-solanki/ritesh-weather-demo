'use strict';

/**
 * @ngdoc overview
 * @name riteshDemoApp
 * @description
 * # riteshDemoApp
 *
 * Main module of the application.
 */
angular
  .module('riteshDemoApp', [
    'config',
    'ngAnimate',
    'ngResource',
    'ngTouch',
    'ui.router',
    'blockUI',
    'chart.js',
  ])
  .config(function ($stateProvider, $urlRouterProvider, blockUIConfig, ChartJsProvider) {
    blockUIConfig.message = 'Please wait...';
    $urlRouterProvider.otherwise('/home');
     $stateProvider
       .state('home', {
        url: '/home',
        views:
        {
          '':
          {
            templateUrl: 'views/home/home.html',
            controller : 'HomeCtrl'
          }
        }
      });
  });