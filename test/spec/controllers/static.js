'use strict';

describe('Controller: StaticCtrl', function () {

  // load the controller's module
  beforeEach(module('aimhireFrontendApp'));

  var StaticCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StaticCtrl = $controller('StaticCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(StaticCtrl.awesomeThings.length).toBe(3);
  });
});
